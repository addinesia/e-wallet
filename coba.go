package main

import (
	"e-wallet/atur"
	"e-wallet/basecon"

	"github.com/gin-gonic/gin"
)

func main() {

	router := gin.Default()

	auth := router.Group("/auth")
	{
		auth.POST("/login", atur.Login)
		auth.POST("/signup", atur.CreateUser)
		auth.PUT("/logout", basecon.Auth, atur.Logout)

	}
	bal := router.Group("/v1/balance")
	{
		bal.POST("/", basecon.Auth, atur.CreateUserBalance)
		bal.GET("/", basecon.Auth, atur.FetchAllBalance)
		bal.POST("/topup", basecon.Auth, atur.TopUPUserBalance)
		bal.POST("/transfer", basecon.Auth, atur.TransferUserBalance)
	}
	ban := router.Group("/v1/bank")
	{
		ban.GET("/", basecon.Auth, atur.FetchAllBankBalance)
		ban.POST("/", basecon.Auth, atur.CreateBankBalance)
		ban.GET("/history/:user_id", basecon.Auth, atur.FetchHistoryBankBalance)
		ban.POST("/topup", basecon.Auth, atur.TopUpBankBalance)
		ban.POST("/transfer", basecon.Auth, atur.TransferBankBalance)
	}
	router.Run()

}
