package basecon

import (
	ent "e-wallet/entities"
	"fmt"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
)

var Db *gorm.DB

func init() {
	conn, err := gorm.Open("postgres", "host=localhost user=golang dbname=wallet_db sslmode=disable password=22")
	// if use docker container
	// conn, err := gorm.Open("postgres", "host=db, user=postgres dbname=postgres sslmode=disable password=example")
	if err != nil {
		fmt.Println(err)
	}
	Db = conn
	Db.Debug().AutoMigrate(&ent.User{}, &ent.UserBalance{}, &ent.UserBlacklist{}, &ent.BankBalance{})

}
