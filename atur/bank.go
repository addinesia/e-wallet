package atur

import (
	"e-wallet/basecon"
	"e-wallet/entities"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

func CreateBankBalance(c *gin.Context) {
	var err error
	userid := c.PostForm("user_id")
	useridC, _ := strconv.Atoi(userid)
	balancebefore := c.PostForm("balance_before")
	balancebeforeC, _ := strconv.Atoi(balancebefore)
	balanceafter := c.PostForm("balance_after")
	balanceafterC, _ := strconv.Atoi(balanceafter)
	get_ip := c.ClientIP()
	get_ug := c.Request.Header.Get("User-Agent")
	p := entities.BankBalance{UserID: useridC, BalanceBefore: balancebeforeC, BalanceAfter: balanceafterC, Activity: c.PostForm("activity"), Type: c.PostForm("type"), IP: get_ip, Location: c.PostForm("location"), UserAgent: get_ug, Author: c.PostForm("author")}

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"errors": err.Error()})
	}
	basecon.Db.Save(&p)
	c.JSON(http.StatusCreated, gin.H{"status": "ok", "result": p})
}

func FetchAllBankBalance(c *gin.Context) {
	var p []entities.BankBalance
	var pt []entities.BankBalanceTamp

	basecon.Db.Find(&p)

	if len(p) <= 0 {
		c.JSON(http.StatusNotFound, gin.H{"status": http.StatusNotFound, "message": "No product found!"})
		return
	}

	for _, item := range p {

		pt = append(pt, entities.BankBalanceTamp{ID: item.ID, UserID: item.UserID, BalanceBefore: item.BalanceBefore, BalanceAfter: item.BalanceAfter, Activity: item.Activity, Type: item.Type, IP: item.IP, Location: item.Location, UserAgent: item.UserAgent, Author: item.Author})
	}
	c.JSON(http.StatusOK, gin.H{"status": http.StatusOK, "data": pt})
}

func FetchHistoryBankBalance(c *gin.Context) {
	var p []entities.BankBalance
	var pt []entities.BankBalanceTamp
	pID := c.Param("user_id")
	search_userid, _ := strconv.Atoi(pID)

	basecon.Db.Where(entities.BankBalance{UserID: search_userid}).First(&p)

	if search_userid == 0 {
		c.JSON(http.StatusNotFound, gin.H{"status": http.StatusNotFound, "message": "No user id found!"})
		return
	}
	for _, item := range p {
		pt = append(pt, entities.BankBalanceTamp{ID: item.ID, UserID: item.UserID, BalanceBefore: item.BalanceBefore, BalanceAfter: item.BalanceAfter, Activity: item.Activity, Type: item.Type, IP: item.IP, Location: item.Location, UserAgent: item.UserAgent, Author: item.Author})
	}

	c.JSON(http.StatusOK, gin.H{"status": http.StatusOK, "data": pt})
}

func TopUpBankBalance(c *gin.Context) {

}

func TransferBankBalance(c *gin.Context) {

}
