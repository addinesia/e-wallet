package atur

import (
	"e-wallet/basecon"
	"e-wallet/entities"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

func CreateUserBalance(c *gin.Context) {
	var err error
	userid := c.PostForm("user_id")
	useridC, _ := strconv.Atoi(userid)
	balancebefore := c.PostForm("balance_before")
	balancebeforeC, _ := strconv.Atoi(balancebefore)
	balanceafter := c.PostForm("balance_after")
	balanceafterC, _ := strconv.Atoi(balanceafter)
	get_ip := c.ClientIP()
	get_ug := c.Request.Header.Get("User-Agent")
	p := entities.UserBalance{UserID: useridC, BalanceBefore: balancebeforeC, BalanceAfter: balanceafterC, Activity: c.PostForm("activity"), Type: c.PostForm("type"), IP: get_ip, Location: c.PostForm("location"), UserAgent: get_ug, Author: c.PostForm("author")}

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"errors": err.Error()})
	}
	basecon.Db.Save(&p)
	c.JSON(http.StatusCreated, gin.H{"status": "ok", "result": p})
}

func FetchAllBalance(c *gin.Context) {
	var p []entities.UserBalance
	var pt []entities.BalanceTamp

	basecon.Db.Find(&p)

	if len(p) <= 0 {
		c.JSON(http.StatusNotFound, gin.H{"status": http.StatusNotFound, "message": "No user id found!"})
		return
	}

	for _, item := range p {

		pt = append(pt, entities.BalanceTamp{ID: item.ID, UserID: item.UserID, BalanceBefore: item.BalanceBefore, BalanceAfter: item.BalanceAfter, Activity: item.Activity, Type: item.Type, IP: item.IP, Location: item.Location, UserAgent: item.UserAgent, Author: item.Author})
	}
	c.JSON(http.StatusOK, gin.H{"status": http.StatusOK, "data": pt})
}

func TopUPUserBalance(c *gin.Context) {
	// b = topup
	// a = select afterbalance where userid ? order limit desc 1
	// hsl := a + b
	// output = post with afterbalance same with hsl and beforebalance same with b

}

func TransferUserBalance(c *gin.Context) {
	// a = select afterbalance where userid ? order limit desc 1
	// b = nom_trans
	// if a < b {cannot transfer}
	// end_sal = a - b
	// own_sal = update afterbalance same with end_sal
	// c = userid
	// dest_sal = select afterbalance where userid = c order limit desc 1
	// hsl = b + dest_sal
	// output = post with afterbalance same with hsl

}
