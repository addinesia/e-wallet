package entities

import (
	"github.com/jinzhu/gorm"
)

type balanceType string

const (
	credit balanceType = "credit"
	debit  balanceType = "debit"
)

type (
	UserBalance struct {
		gorm.Model
		UserID        int    `json:"user_id"`
		BalanceBefore int    `json:"balance_before"`
		BalanceAfter  int    `json:"balance_after"`
		Activity      string `json:"activity"`
		Type          string `json:"type"`
		IP            string `json:"ip"`
		Location      string `json:"location"`
		UserAgent     string `json:"user_agent"`
		Author        string `json:"author"`
	}
	BalanceTamp struct {
		ID            uint   `json:"id"`
		UserID        int    `json:"user_id"`
		BalanceBefore int    `json:"balance_before"`
		BalanceAfter  int    `json:"balance_after"`
		Activity      string `json:"activity"`
		Type          string `json:"type"`
		IP            string `json:"ip"`
		Location      string `json:"location"`
		UserAgent     string `json:"user_agent"`
		Author        string `json:"author"`
	}
)
