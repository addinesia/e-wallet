package entities

import (
	"github.com/jinzhu/gorm"
)

type (
	Login struct {
		Email    string `json:"email"`
		Password string `json:"password"`
	}

	User struct {
		gorm.Model
		Username    string `json:"username"`
		Email       string `json:"email"`
		Password    string `json:"password"`
		UserBalance []UserBalance
		BankBalance []BankBalance
	}

	UserTamp struct {
		ID       uint   `json:"id"`
		Username string `json:"username"`
		Name     string `json:"name"`
		Email    string `json:"email"`
		Password string `json:"password"`
	}

	UserBlacklist struct {
		User
		UserID uint   `json:"userid"`
		JWT    string `gorm:"type:text"`
	}
)
