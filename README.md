# E-Wallet(this is use gin framework)

# Step to run in local
1. Make sure go was install properly
2. Create Database(in this case use PostgreSQL) and set in basecon/conf.go file
3. Run go build -mod vendor
4. Run go run coba.go

# Step to run in container
1. Make sure config database same with environment services db(in file docker-compose.yml)
2. Run docker-compose up
